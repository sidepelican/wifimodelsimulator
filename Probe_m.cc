//
// Generated file, do not edit! Created by nedtool 4.6 from Probe.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#include <iostream>
#include <sstream>
#include "Probe_m.h"

USING_NAMESPACE


// Another default rule (prevents compiler from choosing base class' doPacking())
template<typename T>
void doPacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doPacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}

template<typename T>
void doUnpacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doUnpacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}




// Template rule for outputting std::vector<T> types
template<typename T, typename A>
inline std::ostream& operator<<(std::ostream& out, const std::vector<T,A>& vec)
{
    out.put('{');
    for(typename std::vector<T,A>::const_iterator it = vec.begin(); it != vec.end(); ++it)
    {
        if (it != vec.begin()) {
            out.put(','); out.put(' ');
        }
        out << *it;
    }
    out.put('}');
    
    char buf[32];
    sprintf(buf, " (size=%u)", (unsigned int)vec.size());
    out.write(buf, strlen(buf));
    return out;
}

// Template rule which fires if a struct or class doesn't have operator<<
template<typename T>
inline std::ostream& operator<<(std::ostream& out,const T&) {return out;}

Register_Class(Probe);

Probe::Probe(const char *name, int kind) : ::cPacket(name,kind)
{
    this->macAddr_var = 0;
    this->rssi_var = 0;
    this->sequenceId_var = 0;
    this->deviceType_var = 0;
}

Probe::Probe(const Probe& other) : ::cPacket(other)
{
    copy(other);
}

Probe::~Probe()
{
}

Probe& Probe::operator=(const Probe& other)
{
    if (this==&other) return *this;
    ::cPacket::operator=(other);
    copy(other);
    return *this;
}

void Probe::copy(const Probe& other)
{
    this->macAddr_var = other.macAddr_var;
    this->rssi_var = other.rssi_var;
    this->sequenceId_var = other.sequenceId_var;
    this->deviceType_var = other.deviceType_var;
}

void Probe::parsimPack(cCommBuffer *b)
{
    ::cPacket::parsimPack(b);
    doPacking(b,this->macAddr_var);
    doPacking(b,this->rssi_var);
    doPacking(b,this->sequenceId_var);
    doPacking(b,this->deviceType_var);
}

void Probe::parsimUnpack(cCommBuffer *b)
{
    ::cPacket::parsimUnpack(b);
    doUnpacking(b,this->macAddr_var);
    doUnpacking(b,this->rssi_var);
    doUnpacking(b,this->sequenceId_var);
    doUnpacking(b,this->deviceType_var);
}

const char * Probe::getMacAddr() const
{
    return macAddr_var.c_str();
}

void Probe::setMacAddr(const char * macAddr)
{
    this->macAddr_var = macAddr;
}

double Probe::getRssi() const
{
    return rssi_var;
}

void Probe::setRssi(double rssi)
{
    this->rssi_var = rssi;
}

int Probe::getSequenceId() const
{
    return sequenceId_var;
}

void Probe::setSequenceId(int sequenceId)
{
    this->sequenceId_var = sequenceId;
}

int Probe::getDeviceType() const
{
    return deviceType_var;
}

void Probe::setDeviceType(int deviceType)
{
    this->deviceType_var = deviceType;
}

class ProbeDescriptor : public cClassDescriptor
{
  public:
    ProbeDescriptor();
    virtual ~ProbeDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(ProbeDescriptor);

ProbeDescriptor::ProbeDescriptor() : cClassDescriptor("Probe", "cPacket")
{
}

ProbeDescriptor::~ProbeDescriptor()
{
}

bool ProbeDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<Probe *>(obj)!=NULL;
}

const char *ProbeDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int ProbeDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 4+basedesc->getFieldCount(object) : 4;
}

unsigned int ProbeDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<4) ? fieldTypeFlags[field] : 0;
}

const char *ProbeDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "macAddr",
        "rssi",
        "sequenceId",
        "deviceType",
    };
    return (field>=0 && field<4) ? fieldNames[field] : NULL;
}

int ProbeDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='m' && strcmp(fieldName, "macAddr")==0) return base+0;
    if (fieldName[0]=='r' && strcmp(fieldName, "rssi")==0) return base+1;
    if (fieldName[0]=='s' && strcmp(fieldName, "sequenceId")==0) return base+2;
    if (fieldName[0]=='d' && strcmp(fieldName, "deviceType")==0) return base+3;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *ProbeDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "string",
        "double",
        "int",
        "int",
    };
    return (field>=0 && field<4) ? fieldTypeStrings[field] : NULL;
}

const char *ProbeDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int ProbeDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    Probe *pp = (Probe *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string ProbeDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    Probe *pp = (Probe *)object; (void)pp;
    switch (field) {
        case 0: return oppstring2string(pp->getMacAddr());
        case 1: return double2string(pp->getRssi());
        case 2: return long2string(pp->getSequenceId());
        case 3: return long2string(pp->getDeviceType());
        default: return "";
    }
}

bool ProbeDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    Probe *pp = (Probe *)object; (void)pp;
    switch (field) {
        case 0: pp->setMacAddr((value)); return true;
        case 1: pp->setRssi(string2double(value)); return true;
        case 2: pp->setSequenceId(string2long(value)); return true;
        case 3: pp->setDeviceType(string2long(value)); return true;
        default: return false;
    }
}

const char *ProbeDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    };
}

void *ProbeDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    Probe *pp = (Probe *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}


