#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
#include "VectorHelper.h"
#include "man_m.h"
#include "Probe_m.h"

using namespace VectorHelper;
using namespace std;

class Store : public cSimpleModule
{
    private:
        std::vector<cMessage*> stays; // 店内に滞在中の端末を保持する
        std::vector<cMessage*> waits; // 外で待ってるの端末を保持する
        map<int,vector<Man*> > waitingGroupMembers; // 待っている先頭集団がグループの場合，そのメンバー

        int serviceTimeMean;
        int serviceTimeStddev;
        int serviceCapacity;
        float reappearanceRate;

    protected:
        virtual void initialize();
        virtual void handleMessage(cMessage *msg);
        virtual void finish();

        virtual float genStayTime(Man* senderMan);
        virtual void  sendProbe(Man* senderMan);
        Probe* genProbeRequest(Man* senderMan);
        Probe* genProbeRequest(Probe* base, double deviation);

        virtual void arriveNewMan(Man* mmsg);
        virtual void leftStayedMan(Man* mmsg);
        virtual void enterNewMan(Man* mmsg);
        virtual void refreshDisplayLabel();
};

Define_Module(Store);


void Store::initialize()
{
    // シミュレートパラメータの取得
    serviceTimeMean   = par("serviceTimeMean").longValue();
    serviceTimeStddev = par("serviceTimeStddev").longValue();
    serviceCapacity   = par("serviceCapacity").longValue();
    reappearanceRate  = par("reappearanceRate").doubleValue();
}

void Store::finish()
{
    // 滞在中の人を全てリリース
    if(stays.size() > 0){
        for (auto man : stays) cancelAndDelete(man);
    }
    if(waits.size() > 0){
        for (auto man : waits) cancelAndDelete(man);
    }

    // 適当に結果を出力してみる
    EV << "duration: " << simTime() << endl;

    recordScalar("duration", simTime());
}

float Store::genStayTime(Man* senderMan)
{
    // 正規分布で生成
    auto staytime = normal(serviceTimeMean,serviceTimeStddev);

    // グループオプション付きユーザの場合，滞在時間を統一

    return staytime > 0? staytime: genStayTime(senderMan);
//    return uniform(serviceTimeMin,serviceTimeMax);
}

void Store::leftStayedMan(Man* mmsg)
{
//    mmsg->setLeftTime(simTime()); // 帰る時間を記録 -> 入店時に帰る時間を生成するので不要

    // 一定確率で人が再登場する（同じMACアドレスの人を生成する）
    if (uniform(0,1) < reappearanceRate) {
        auto mes = new cMessage(mmsg->getName());
        send(mes, "gate$o");
    }

    // 帰る処理
    erase(stays, (cMessage*)mmsg);
    send(mmsg, "gone$o");

    // 待ちの人がいる場合、その人を入店させてあげる
    if(waits.size() > 0){
        Man* frontCustomer = (Man*)waits.front();

        frontCustomer->setWaitedTime(simTime() - frontCustomer->getArrivedTime());// 待った時間を記録
        enterNewMan(frontCustomer);

        erase(waits,(cMessage*)frontCustomer);
    }
}

void Store::arriveNewMan(Man* mmsg)
{
    mmsg->setArrivedTime(simTime()); // 到着時間を記録

    // 店内に入れるか検証、入れる場合
    if (stays.size() < serviceCapacity){
        enterNewMan(mmsg);
    }else{
        // 入れない場合
        waits.push_back(mmsg);
    }

    // 到着時のプローブ要求送信＆定期的にプローブ要求を送信するようスケジュールする
    sendProbe(mmsg);
}

void Store::enterNewMan(Man* mmsg)
{
    // グループユーザの場合，一旦ここでスタック & リリース
    if (mmsg->getOptions() & kOptionsHasGroup) {
        int groupId = mmsg->getGroupingId();
        waitingGroupMembers[groupId].push_back(mmsg);
        // 人数が揃った時，入店させる．揃ってない場合は何もせず次の入店タイミング（既存客が離れるとき）を待つ
        if (waitingGroupMembers[groupId].size() == mmsg->getGroupSize()) {
            // まとめて入店処理．帰る時間はいっしょ
            simtime_t leftTime = simTime() + genStayTime(mmsg);
            for (Man* user : waitingGroupMembers[groupId]) {
                stays.push_back(user);
                user->setLeftTime(leftTime);
            }
            waitingGroupMembers.erase(groupId);
        }
    }else{
        // 1人で来た場合
        stays.push_back(mmsg);
        simtime_t leftTime = simTime() + genStayTime(mmsg);
        mmsg->setLeftTime(leftTime);
    }

}

Probe* Store::genProbeRequest(Man* senderMan)
{
    auto ret = new Probe();
    ret->setMacAddr(senderMan->getName());
    ret->setRssi(uniform(-90,-50));
    int currentID = senderMan->getCurrentSeqeuenceId() + 1; // シーケンス番号を更新
    senderMan->setCurrentSeqeuenceId(currentID);
    ret->setSequenceId(currentID);
    return ret;
}

Probe* Store::genProbeRequest(Probe* base, double deviation)
{
    if (base == nullptr) return nullptr;
    double rssi = base->getRssi() + deviation;

    auto ret = new Probe();
    ret->setMacAddr(base->getMacAddr());
    ret->setRssi(rssi);
    ret->setSequenceId(base->getSequenceId()); // こちらではシーケンス番号を更新しない
    return ret;
}

void Store::sendProbe(Man* sender)
{
    // プローブ要求を送信する
    Probe* insideProbe = nullptr, *outsideProbe = nullptr;
    std::string nodeName = std::string(getName());
    if (nodeName == "store"){
        insideProbe = genProbeRequest(sender);
        // 待っている人はやや外側APに寄りやすくする
        if (contain(waits, sender)) {
            outsideProbe = genProbeRequest(insideProbe, uniform(-8, +5));
        }else{
            outsideProbe = genProbeRequest(insideProbe, uniform(-10, +3));
        }
    }else if (nodeName == "passing") {
        insideProbe = genProbeRequest(sender);
        outsideProbe = genProbeRequest(insideProbe, uniform(-3, 10));
    }

    // オプション処理
    unsigned int manOptions = sender->getOptions();
    if (manOptions & kOptionsNoPhones) { // スマホを持っていないユーザ
        insideProbe = outsideProbe = NULL;
    }else if (manOptions & kOptionsDoublePhones) { // スマホを２台持っているユーザ
        Probe* subTerminalProbe1 = genProbeRequest(insideProbe, uniform(-2, 2));
        Probe* subTerminalProbe2 = genProbeRequest(outsideProbe, uniform(-2, 2));
        ASSERT(subTerminalProbe2); // プログラム簡略化のためNULLエスケープしません．
        const char* subMac = (std::string(subTerminalProbe1->getMacAddr()) + "-2").c_str();
        subTerminalProbe1->setMacAddr(subMac);
        subTerminalProbe2->setMacAddr(subMac);
        send(subTerminalProbe1, "prbout", 0);
        send(subTerminalProbe2, "prbout", 1);
    }

    // 実際の送信
    if(insideProbe){send(insideProbe, "prbout", 0);}
    if(outsideProbe){send(outsideProbe, "prbout", 1);}

    // 次のプローブ送信コールをスケジュール
    simtime_t nextTime = simTime() + sender->getProbeInterval();
    simtime_t leftTime = sender->getLeftTime();
    if (leftTime != 0 && nextTime > leftTime){
        nextTime = leftTime;
    }// 帰る時間を過ぎるならそれに合わせてスケジュール。これでプローブのインターバル間に帰る時間が来ても正しい時間に帰れる
    scheduleAt(nextTime, sender);
}

void Store::handleMessage(cMessage *msg)
{
    Man *mmsg = check_and_cast<Man *>(msg);
    if (contain(stays, msg)){
        if(mmsg->getLeftTime() <= simTime()){
            // 滞在してた端末が帰る場合
            leftStayedMan(mmsg);
        }else{
            // プローブ送信コールの場合、プローブ要求を送る
            sendProbe(mmsg);
        }

    }else if (contain(waits, msg)){
        // プローブ要求を送る
        sendProbe(mmsg);

    }else{
        Man *mmsg = check_and_cast<Man *>(msg);
        // 新しくやってきた端末の場合
        arriveNewMan(mmsg);
    }

    // ディスプレイラベルの更新
    refreshDisplayLabel();
}

void Store::refreshDisplayLabel()
{
    if (ev.isGUI()){
        char ms[20];
        sprintf(ms,"Waiting: %d, InService: %d",(int)waits.size(),(int)stays.size());
        getDisplayString().setTagArg("t",0,ms);
    }
}
