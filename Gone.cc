#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
#include "man_m.h"

#include<fstream>
#include<iostream>
using namespace std;

class Gone : public cSimpleModule
{
  private:
    int startHour;
    std::vector<std::string> resultLines;
    string simTimetoString(const simtime_t input, bool addStartTime = true);

  protected:
    virtual void initialize();
    virtual void finish();
    virtual void handleMessage(cMessage *msg);
};

Define_Module(Gone);


void Gone::initialize()
{
    // シミュレート開始時間を記録
    startHour = getParentModule()->par("sim_start_hour");
}

void Gone::finish()
{
    // シミュレート結果の保存 (QRコードで収集した形式)
    ofstream ofs("/Users/kenta/Desktop/ap_tmp/true.csv");
    for (int i=0; i < resultLines.size(); i++){
        ofs << resultLines[i] << endl;
    }

}

std::string Gone::simTimetoString(const simtime_t input, bool addStartTime)
{
    double simtime = input.dbl();
    int hh = simtime / 60;
    if(addStartTime)hh += startHour;
    int mm = (int)simtime % 60;
    int ss = ((int)(simtime*10) % 10) * 6;
    char tmp[10];
    sprintf(tmp, "%d:%02d:%02d",hh,mm,ss);
    std::string timestring(tmp);
    return timestring;
}

void Gone::handleMessage(cMessage *msg)
{
//    bubble("Catch!");
    Man *mmsg = check_and_cast<Man *>(msg);

    // GUI更新
    if(ev.isGUI()){
        simtime_t totalTime = mmsg->getLeftTime() - mmsg->getArrivedTime();

        char ms[20];
        sprintf(ms,"total %d s,waited %d s",(int)totalTime.dbl(),(int)mmsg->getWaitedTime().dbl());
        bubble(ms);
    }

    // 集まった時間データを配列に保存
    if (mmsg->getStaytype() == kStayTypeInService){
        string arrive   = simTimetoString(mmsg->getArrivedTime());
        string enter    = simTimetoString(mmsg->getArrivedTime() + mmsg->getWaitedTime());
        string waited   = simTimetoString(mmsg->getWaitedTime(), false);
        string left     = simTimetoString(mmsg->getLeftTime());
        string mac      = string(mmsg->getName());
        resultLines.push_back(arrive + "," + enter + "," + waited + "," + left + "," + mac);
    }

    delete msg;
}

