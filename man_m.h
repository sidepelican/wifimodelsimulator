//
// Generated file, do not edit! Created by nedtool 4.6 from man.msg.
//

#ifndef _MAN_M_H_
#define _MAN_M_H_

#include <omnetpp.h>

// nedtool version check
#define MSGC_VERSION 0x0406
#if (MSGC_VERSION!=OMNETPP_VERSION)
#    error Version mismatch! Probably this file was generated by an earlier version of nedtool: 'make clean' should help.
#endif



/**
 * Enum generated from <tt>man.msg:2</tt> by nedtool.
 * <pre>
 * enum kStayType
 * {
 * 
 *     kStayTypeInService = 0;
 *     kStayTypeMoment = 1;
 *     kStayTypeStatic = 2;
 * }
 * </pre>
 */
enum kStayType {
    kStayTypeInService = 0,
    kStayTypeMoment = 1,
    kStayTypeStatic = 2
};

/**
 * Enum generated from <tt>man.msg:8</tt> by nedtool.
 * <pre>
 * enum kOptions
 * {
 * 
 *     kOptionsNoPhones = 1; // 1 << X が使えない．不便
 *     kOptionsDoublePhones = 2;
 *     kOptionsHasGroup = 4;
 * }
 * </pre>
 */
enum kOptions {
    kOptionsNoPhones = 1,
    kOptionsDoublePhones = 2,
    kOptionsHasGroup = 4
};

/**
 * Class generated from <tt>man.msg:14</tt> by nedtool.
 * <pre>
 * message Man
 * {
 *     simtime_t arrivedTime;
 *     simtime_t leftTime;
 *     simtime_t waitedTime = 0;
 *     int staytype;
 *     int currentSeqeuenceId = 0;
 *     double probeInterval;
 *     unsigned int options = 0;
 *     int groupingId;
 *     int groupSize;
 * }
 * </pre>
 */
class Man : public ::cMessage
{
  protected:
    simtime_t arrivedTime_var;
    simtime_t leftTime_var;
    simtime_t waitedTime_var;
    int staytype_var;
    int currentSeqeuenceId_var;
    double probeInterval_var;
    unsigned int options_var;
    int groupingId_var;
    int groupSize_var;

  private:
    void copy(const Man& other);

  protected:
    // protected and unimplemented operator==(), to prevent accidental usage
    bool operator==(const Man&);

  public:
    Man(const char *name=NULL, int kind=0);
    Man(const Man& other);
    virtual ~Man();
    Man& operator=(const Man& other);
    virtual Man *dup() const {return new Man(*this);}
    virtual void parsimPack(cCommBuffer *b);
    virtual void parsimUnpack(cCommBuffer *b);

    // field getter/setter methods
    virtual simtime_t getArrivedTime() const;
    virtual void setArrivedTime(simtime_t arrivedTime);
    virtual simtime_t getLeftTime() const;
    virtual void setLeftTime(simtime_t leftTime);
    virtual simtime_t getWaitedTime() const;
    virtual void setWaitedTime(simtime_t waitedTime);
    virtual int getStaytype() const;
    virtual void setStaytype(int staytype);
    virtual int getCurrentSeqeuenceId() const;
    virtual void setCurrentSeqeuenceId(int currentSeqeuenceId);
    virtual double getProbeInterval() const;
    virtual void setProbeInterval(double probeInterval);
    virtual unsigned int getOptions() const;
    virtual void setOptions(unsigned int options);
    virtual int getGroupingId() const;
    virtual void setGroupingId(int groupingId);
    virtual int getGroupSize() const;
    virtual void setGroupSize(int groupSize);
};

inline void doPacking(cCommBuffer *b, Man& obj) {obj.parsimPack(b);}
inline void doUnpacking(cCommBuffer *b, Man& obj) {obj.parsimUnpack(b);}


#endif // ifndef _MAN_M_H_

