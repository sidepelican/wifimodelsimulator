//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <stdio.h>
#include <string.h>

#ifndef VECTORHELPER_H_
#define VECTORHELPER_H_

namespace VectorHelper {


    template<class T>
    bool contain(const std::vector<T>& vec, void* target){
        if(vec.empty())return false;
        return std::find(vec.begin(),vec.end(), target) != vec.end();
    }

    template<class T>
    bool contain(const std::vector<T>& vec, T target){
        if(vec.empty())return false;
        return std::find(vec.begin(),vec.end(), target) != vec.end();
    }

    template<class T>
    void erase(std::vector<T>& vec, void* target){
        if(vec.empty()) return;
        auto pos = std::find(vec.begin(),vec.end(), target);
        if(pos == vec.end()) return;
        vec.erase(pos);
    }

    template<class T>
    void erase(std::vector<T>& vec, T target){
        if(vec.empty()) return;
        auto pos = std::find(vec.begin(),vec.end(), target);
        if(pos == vec.end()) return;
        vec.erase(pos);
    }
}

#endif /* VECTORHELPER_H_ */
