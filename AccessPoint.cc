#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
#include "Probe_m.h"

#include<fstream>
#include<iostream>
using namespace std;

class AccessPoint : public cSimpleModule
{
  private:
//    vector<string> resultTime;
//    vector<string> resultMAC;
//    vector<double> resultRSSI;
    std::vector<std::string> resultLines;

    int startHour, endHour;

  protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
    virtual void finish();
};

Define_Module(AccessPoint);


void AccessPoint::initialize()
{
    // シミュレート開始時間と終了時間を記録
    startHour = getParentModule()->par("sim_start_hour");
    endHour   = getParentModule()->par("sim_end_hour");
}

void AccessPoint::finish()
{
    // シミュレート結果の保存
    std::string deviceName = std::string(getName());
    ofstream ofs("/Users/kenta/Desktop/ap_tmp/" + deviceName + ".csv");
//    for (int i=0; i < resultTime.size(); i++){
//        ofs << resultTime[i] << "," << resultRSSI[i] << "," << resultMAC[i] << "," << deviceName << endl;
//    }
    for (std::string line : resultLines) {
        ofs << line << endl;
    }

}

void AccessPoint::handleMessage(cMessage *msg)
{

    // simtime()を現実らしい値に変換
    double simtime = simTime().dbl();
    int hh = simtime / 60 + startHour;
    int mm = (int)simtime % 60;
    int ss = ((int)(simtime*10) % 10) * 6;
    char tmp[10];
    sprintf(tmp, "%d:%02d:%02d",hh,mm,ss);
    std::string timestring(tmp);

    // 時間すぎてたら終了
    if(hh >= endHour)endSimulation();

    // 結果出力用配列に行挿入
    Probe* pmsg = check_and_cast<Probe*>(msg);
    std::string deviceName = std::string(getName());
    std::string rssi       = std::to_string(pmsg->getRssi());
    std::string seqid      = std::to_string(pmsg->getSequenceId());
    resultLines.push_back(timestring + "," + rssi + "," + pmsg->getMacAddr() + "," + deviceName + "," + seqid);
//    resultTime.push_back(timestring);
//    resultRSSI.push_back(pmsg->getRssi());
//    resultMAC.push_back(pmsg->getMacAddr());

    delete msg;
}

