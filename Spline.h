#ifndef SPLINE_H_
#define SPLINE_H_

/********************************/
/* 補間法（３次スプライン関数） */
/*      coded by Y.Suganuma     */
/********************************/
#include <stdio.h>

/****************/
/* クラスの定義 */
/****************/
class Spline
{
        double *h, *b, *d, *g, *u, *r, *q, *s, *x, *y;
        int n;

    public:

        /**************************/
        /* コンストラクタ         */
        /*      n1 : 区間の数     */
        /*      x1, y1 : 点の座標 */
        /**************************/
        Spline(int n1, double *x1, double *y1)
        {
            int i1;
                        // 領域の確保
            n = n1;

            h = new double [n];
            b = new double [n];
            d = new double [n];
            g = new double [n];
            u = new double [n];
            q = new double [n];
            s = new double [n];
            r = new double [n+1];
            x = new double [n+1];
            y = new double [n+1];

            for (i1 = 0; i1 <= n; i1++) {
                x[i1] = x1[i1];
                y[i1] = y1[i1];
            }
                        // ステップ１
            for (i1 = 0; i1 < n; i1++)
                h[i1] = x[i1+1] - x[i1];
            for (i1 = 1; i1 < n; i1++) {
                b[i1] = 2.0 * (h[i1] + h[i1-1]);
                d[i1] = 3.0 * ((y[i1+1] - y[i1]) / h[i1] - (y[i1] - y[i1-1]) / h[i1-1]);
            }
                        // ステップ２
            g[1] = h[1] / b[1];
            for (i1 = 2; i1 < n-1; i1++)
                g[i1] = h[i1] / (b[i1] - h[i1-1] * g[i1-1]);
            u[1] = d[1] / b[1];
            for (i1 = 2; i1 < n; i1++)
                u[i1] = (d[i1] - h[i1-1] * u[i1-1]) / (b[i1] - h[i1-1] * g[i1-1]);
                        // ステップ３
            r[0]   = 0.0;
            r[n]   = 0.0;
            r[n-1] = u[n-1];
            for (i1 = n-2; i1 >= 1; i1--)
                r[i1] = u[i1] - g[i1] * r[i1+1];
                        // ステップ４
            for (i1 = 0; i1 < n; i1++) {
                q[i1] = (y[i1+1] - y[i1]) / h[i1] - h[i1] * (r[i1+1] + 2.0 * r[i1]) / 3.0;
                s[i1] = (r[i1+1] - r[i1]) / (3.0 * h[i1]);
            }

        }

        /****************/
        /* デストラクタ */
        /****************/
        ~Spline()
        {
            delete [] h;
            delete [] b;
            delete [] d;
            delete [] g;
            delete [] u;
            delete [] q;
            delete [] s;
            delete [] r;
            delete [] x;
            delete [] y;
        }

        /******************************/
        /* 補間値の計算               */
        /*      x1 : 補間値を求める値 */
        /*      return : 補間値       */
        /******************************/
        double value(double x1)
        {
            int i = -1, i1;
            double y1, xx;
                        // 区間の決定
            for (i1 = 1; i1 < n && i < 0; i1++) {
                if (x1 < x[i1])
                    i = i1 - 1;
            }
            if (i < 0)
                i = n - 1;
                        // 計算
            xx = x1 - x[i];
            y1 = y[i] + xx * (q[i] + xx * (r[i]  + s[i] * xx));

            return y1;
        }

};

#endif /* SPLINE_H_ */
