#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
#include "VectorHelper.h"
#include "man_m.h"
#include "Spline.h"

using namespace VectorHelper;

class Phone : public cSimpleModule
{
    private:
        const simtime_t deltaTime = 10;

        double  enterRate;
        double  scatterRate;
        int     staticDevicesCount;

        double visiterValue(simtime_t);

        std::vector<cMessage*> scheduledEvents;
        std::vector<cMessage*> reappearenceMACs;
        cMessage* scheduleCall;
        cOutVector gendCountVector;
    protected:
        virtual void initialize();
        virtual void handleMessage(cMessage *msg);
        virtual int  scheduleEvents();
        virtual void scheduleStaticEvents();

        virtual Man* genUniqueMan();

    public:
        Phone();
        virtual ~Phone();
};

Define_Module(Phone);


Phone::Phone()
{
    scheduleCall = NULL;
}
Phone::~Phone()
{
    cancelAndDelete(scheduleCall);
}

void Phone::initialize()
{
    // パラメータ取得
    enterRate           = par("enterRate").doubleValue();
    scatterRate         = par("scatterRate").doubleValue();
    staticDevicesCount  = par("staticDevices").longValue();

    // 単位時間ごとにhandleMessageを呼び出すためのイベントの初期化
    scheduleCall = new cMessage("");
    scheduleAt(deltaTime, scheduleCall);

    scheduleEvents();
    scheduleStaticEvents();
}

double Phone::visiterValue(simtime_t simtime)
{
    // 人の訪れる割合を設定

     int n = 4;// 生成するスプライン曲線の解像度

     double* x = new double [n+1];
     double* y = new double [n+1];
     for(int i=0;i<n+1;i++)x[i]=i;
     y[0] = 100;
     y[1] = 120;
     y[2] = 80;
     y[3] = 40;
     y[4] = 20;

     // 現在時刻が全体の中でどの位置にいるか計算

     int startHour = getParentModule()->par("sim_start_hour");
     int endHour   = getParentModule()->par("sim_end_hour");

     double deltaTime = simtime.dbl() / ((endHour - startHour)*60);// 現在時刻 / 全体時刻
     return Spline(n, x, y).value(deltaTime * n) * (1 + uniform(scatterRate, - scatterRate));
}

void Phone::scheduleStaticEvents()
{
    // TODO: 最初に適当な数送ってるだけだけど果たしてこれでいいのか
    for (int i=0;i<staticDevicesCount;i++){
        Man* staticman = genUniqueMan();
        staticman->setStaytype(kStayTypeStatic);
        sendDelayed(staticman, uniform(0,deltaTime), "gate$o", kStayTypeStatic);
    }
}

int Phone::scheduleEvents()
{
    // 現行時間から現行時間＋単位時間までの間のイベントをスケジュールする

    int spawnEventsCount = visiterValue(simTime()) / 2; // グループユーザ導入により1Eventあたりの出現ユーザ数期待値が2になったので，その分を相殺する
    // TODO: ここきちんと出現ユーザ数期待値を計算する

    for (int i=0;i<spawnEventsCount;i++){
        auto newevent = new cMessage("I'm user call event.");
        scheduleAt(simTime() + uniform(0,deltaTime) , newevent);
        scheduledEvents.push_back(newevent);
    }

    // GUIの更新
    if (ev.isGUI()){
        char ms[30];
        sprintf(ms,"Send %d Users in %d s",spawnEventsCount,(int)deltaTime.dbl());
        getDisplayString().setTagArg("t",0,ms);
        bubble("Gend next Users");
    }

    gendCountVector.record(spawnEventsCount);
    return spawnEventsCount;
}

// 端末ごとの固有IDを生成する(MACアドレスの代わり)
Man* Phone::genUniqueMan()
{
    static int aaa = 0;
    aaa ++;

    // 名前をつけて新しいManを生成
    char msgname[20];
    sprintf(msgname, "MAC%d", aaa);
    auto ret = new Man(msgname);

    // プローブ要求の送信間隔を設定
    ret->setProbeInterval(uniform(1,4)); // 適当

    // 確立でスマホを持たないor２つ持ち
    if (uniform(0,1) < 0.08) {
        switch (intuniform(0,1)) {
        case 0:
            ret->setOptions(kOptionsNoPhones & ret->getOptions());
            break;
        default:
            ret->setOptions(kOptionsDoublePhones & ret->getOptions());
        }
    }

    // 再登場スタックになにか存在する場合，確立でそれを引っ張りだす
    if (reappearenceMACs.size() > 0 && uniform(0,1) < 0.08) {
        int randIndex = intuniform(0,reappearenceMACs.size()-1); //out of rangeに注意

        auto reappearMes = reappearenceMACs[randIndex];
//        EV << "reappear!: " << reappearMes->getName() << reappearMes;
        ret->setName(std::string(reappearMes->getName()).c_str());

        reappearenceMACs.erase(reappearenceMACs.begin() + randIndex);
        delete reappearMes;
    }

    // 行動パターンを設定
    if (uniform(0,1) < enterRate){
        ret->setStaytype(kStayTypeInService);
    }else{
        ret->setStaytype(kStayTypeMoment);
    }

    return ret;
}

void Phone::handleMessage(cMessage *msg)
{
    // 単位時間ごとの人が来るのをスケジュールするイベント呼び出し
    if (msg == scheduleCall){
        scheduleEvents();
        scheduleAt(simTime()+deltaTime,scheduleCall);

        return;
    }

    // スケジュールされたイベントを送信する
    if (contain(scheduledEvents, msg)){
        std::vector<Man*>users;
        // とりあえず一人生成
        Man* baseMan = genUniqueMan();
        users.push_back(baseMan);
        // 確立でグループを生成する
        if (uniform(0,1) < 0.5) {
            // 子分ユーザを追加
            int groupsize = intuniform(2,4);
            while (users.size() < groupsize) {
                Man* another = genUniqueMan();
                users.push_back(another);
            }

            // 行き先を統一，グループID付与
            static int groupId = 0; groupId++;
            for (auto user : users) {
                user->setStaytype(baseMan->getStaytype());
                user->setGroupingId(groupId);
                user->setOptions(user->getOptions() & kOptionsHasGroup);
                user->setGroupSize(groupsize);
            }
        }

        for (auto user : users) {
            send(user, "gate$o", user->getStaytype());
        }

        erase(scheduledEvents, msg);
        delete msg;
    }else{
        // 再登場（reappearence）する人の場合
        reappearenceMACs.push_back(msg);
    }
}
